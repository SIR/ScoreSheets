Languages: ***[English](README.md)*** **Deutsch**

# Spieleblock

## Allgemein
Spieleblock ist eine Android-App zum Erfassen und Berechnen von Ergebnissen für folgende Spiele:

* Rommé
* Sechsundsechzig
* das bekannte Spiel Sk... King (hier Piraten)
* das bekannte Spiel Wizard®
* das Würfelspiel Kn...el (hier Pasch)
* und alle anderen Spiele mit einfacher Wertung duch Addition/Subtraktion

## Funktionen
* unterstützte Sprachen: englisch, deutsch
* bis zu 8 Spieler (je nach Spiel)
* unbegrenzte Anzahl vordefinierter Spielernamen
* Rommé: doppelte Wertung bei Hand-Rommé mit einem Klick
* automatische Berechnung
* Markierung für den Geber
* **keine Berechtigungen erforderlich**

## Screenshots
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/1.png" alt="Screenshot Startbildschirm" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/2.png" alt="Screenshot Sechsundsechzig" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/3.png" alt="Screenshot Piraten" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/4.png" alt="Screenshot Pasch" width="200"/>

## Downloads

Downloads werden bereitgestellt von:

* [Codeberg](https://codeberg.org/SIR/ScoreSheets/releases)
* [F-Droid](https://f-droid.org/de/packages/de.beisteiners.spieleblock/) (the F-Droid app can be downloaded [here](https://f-droid.org/))
* [Play Store](https://play.google.com/store/apps/details?id=de.beisteiners.spieleblock)

**Wichtig**: Die Codeberg-Version wird häufiger aktualisiert als die F-Droid-Version.  
F-Droid erstellt in unregelmäßigen Abständen neue Versionen und Sie benötigen den F-Droid-Client, um Update-Benachrichtigungen zu erhalten.

## Bemerkungen
Diese Android-App wurde für eigene private Zwecke erstellt, um die "Zettelwirtschaft" zu beseitigen. Vielleicht ist sie auch für andere nützlich.

Wizard® ist ein registriertes Markenzeichen von Ken Fisher und er gab mir das Recht, den Name hier zu verwenden. Vielen Dank dafür!

**Sollten Sie einen Verstoß gegen Markenrechte entdecken, öffnen Sie bitte einen Issue.**