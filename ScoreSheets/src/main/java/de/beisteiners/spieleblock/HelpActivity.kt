/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Ronny Steiner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.beisteiners.spieleblock

import android.graphics.Typeface
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat

class HelpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        val helpTextView: TextView=findViewById(R.id.help_text)
        helpTextView.typeface= Typeface.createFromAsset(applicationContext.assets,"icons.ttf")
        helpTextView.text = HtmlCompat.fromHtml(replaceIcons(getString(R.string.help)),0)
        val mybutton = findViewById(R.id.toolbar_help) as androidx.appcompat.widget.Toolbar
        mybutton.setOnClickListener {
            finish()
        }
    }
    fun replaceIcons (htmlString: String): String {
        var returnString=htmlString.replace("[score]","&#x00a1;").
        replace("[double_score]","&nbsp;&#x00a2;&nbsp;").
        replace("[start_game]","&nbsp;&#x00a3;&nbsp;").
        replace("[finish_game]","&nbsp;&#x00a4;&nbsp;").
        replace("[add_player]","&nbsp;&#x00a5;&nbsp;").
        replace("[remove_player]","&nbsp;&#x00a6;&nbsp;").
        replace("[restart_game]","&nbsp;&#x00a7;&nbsp;").
        replace("[yo_ho_ho]","&nbsp;&#x00a8;&nbsp;")
        return returnString
    }
}