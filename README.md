Languages: **English** ***[Deutsch](README.de.md)***

# Score sheets

## General
Score sheets is an Android app to capture and calculate results for the following games:

* Rummy
* Sixty-Six
* popular game Sk... King (here Pirates)
* popular game Wizard®
* dice game Y...zee (here Pasch)
* and other games where the points must be added or substracted

## Features
* supported languages: english, german
* up to 8 players
* unlimited predefined player names
* Rummy: add double score with one click
* automatic calculation
* dealer indicator
* **does not require any permissions**

## Screenshots
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" alt="Screenshot Home" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" alt="Screenshot Sixty-Six" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" alt="Screenshot Pirates" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" alt="Screenshot Pasch" width="200"/>

## Downloads

Supported download locations:

* [Codeberg](https://codeberg.org/SIR/ScoreSheets/releases)
* [F-Droid](https://f-droid.org/en/packages/de.beisteiners.spieleblock/) (the F-Droid app can be downloaded [here](https://f-droid.org/))
* [Play Store](https://play.google.com/store/apps/details?id=de.beisteiners.spieleblock)

**Important**: The Codeberg version is being updated more often than the F-Droid version.
F-Droid builds new versions irregularly and you'll need the F-Droid client to get update notifications.

## Remarks
I made this Android app for my own private use. Maybe someone else will find it useful.

Wizard® is a registered trademark by Ken Fisher and he gave me the right to use the name of the game. Thank you!

**If you suspect a violation of trademark law, please open an issue.**